import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { AuthModule } from './generic-auth/auth.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost/generic-auth'),
    ConfigModule.forRoot({
      envFilePath: `./environments/.env`,
      isGlobal: true,
    }),
    AuthModule.register({
      authenticationtype: ['self', 'google'],
      jwtModuleOptions: {
        secret: process.env.JWT_SECRET,
        signOptions: {
          expiresIn: process.env.JWT_EXPIRES,
        },
      },
      passwordReset: true,
      passwordResetExpires: 10,
      accountActivation: true,
      emailOptions: {
        emailFrom: process.env.EMAIL_FROM,
        emailHost: process.env.EMAIL_HOST,
        emailPort: +process.env.EMAIL_PORT,
        emailUser: process.env.EMAIL_USER,
        emailPassword: process.env.EMAIL_PASSWORD,
        passwordResetUrl: process.env.PASSWORD_RESET_URL,
        accountActivationUrl: process.env.PASSWORD_ACTIVATION_URL,
      },
      googleAuthOptions: {
        scope: ['https://www.googleapis.com/auth/userinfo.email'],
        clientId: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        redirectUrl: process.env.GOOGLE_REDIRECT_URL,
        authSuccessRedirectUrl: process.env.GOOGLE_AUTH_SUCCESS_URL,
        authFailedRedirectUrl: process.env.GOOGLE_AUTH_FAILED_URL,
      },
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
