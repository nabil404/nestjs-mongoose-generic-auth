import { JwtModuleOptions } from '@nestjs/jwt';
import { IAuthModuleOptions } from '@nestjs/passport';

export interface AuthOptions {
  authenticationtype: string[];
  passportModuleOptions?: IAuthModuleOptions;
  jwtModuleOptions: JwtModuleOptions;
  defaultRoles?: string[];
  connectionName?: string;
  passwordReset?: boolean;
  passwordResetExpires?: number;
  accountActivation?: boolean;
  accountActivationExpires?: number;
  emailOptions?: {
    emailFrom: string;
    emailHost: string;
    emailPort?: number;
    emailUser: string;
    emailPassword: string;
    passwordResetUrl?: string; //www.example.com/reset/{{token}}
    passwordResetSubject?: string;
    passwordResetTemplate?: string; //<a href={{resetUrl}}>{{resetUrl}}</a>
    accountActivationUrl?: string; //www.example.com/activate/{{token}}
    accountActivationSubject?: string;
    accountActivationTemplate?: string; //<a href={{activationUrl}}>{{activationUrl}}</a>
  };
  googleAuthOptions?: {
    scope: string[];
    clientId: string;
    clientSecret: string;
    redirectUrl: string;
    authSuccessRedirectUrl: string;
    authFailedRedirectUrl: string;
  };
}
