import {
  HttpException,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { PassportStrategy } from '@nestjs/passport';
import { Request } from 'express';
import { Model } from 'mongoose';
import { Strategy } from 'passport-jwt';
import { AUTH_OPTIONS } from '../constants';
import { AuthOptions } from '../interfaces/auth-options.interface';
import { User, UserDocument } from '../schema/user.schema';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  authOptions: AuthOptions;

  constructor(
    @InjectModel(User.name) private readonly user: Model<UserDocument>,
    @Inject(AUTH_OPTIONS)
    authOptions: AuthOptions,
  ) {
    super({
      jwtFromRequest: (req: Request) => req.cookies['token'],
      ignoreExpiration: false,
      secretOrKey: authOptions.jwtModuleOptions.secret,
    });

    this.authOptions = authOptions;
  }

  async validate(payload): Promise<User> {
    const { email } = payload;
    const user = await this.user.findOne({ email }).select('-password');
    if (!user) throw new UnauthorizedException();
    if (this.authOptions.accountActivation) {
      if (!user.isActivated)
        throw new HttpException('account not activated', 400);
    }

    return {
      email: user.email,
      roles: user.roles,
      authType: user.authType,
      isActivated: user.isActivated,
    };
  }
}
