import { Inject, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { Response } from 'express';
import { Model } from 'mongoose';
import { google } from 'googleapis';
import { User, UserDocument } from '../schema/user.schema';
import { AUTH_OPTIONS } from '../constants';
import { AuthOptions } from '../interfaces/auth-options.interface';

@Injectable()
export class GoogleAuthService {
  oAuth2Client: any;
  authUrl: string;
  private authType: string = 'google';
  constructor(
    @InjectModel(User.name) private readonly user: Model<UserDocument>,
    @Inject(AUTH_OPTIONS) private authOptions: AuthOptions,
    private jwtService: JwtService,
  ) {
    this.oAuth2Client = new google.auth.OAuth2(
      this.authOptions.googleAuthOptions.clientId,
      this.authOptions.googleAuthOptions.clientSecret,
      this.authOptions.googleAuthOptions.redirectUrl,
    );
    this.authUrl = this.oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: this.authOptions.googleAuthOptions.scope,
    });
  }

  private async getGoogleUserEmail(code: string): Promise<string | null> {
    const { tokens } = await this.oAuth2Client.getToken(code);
    this.oAuth2Client.setCredentials(tokens);
    const profile = google.oauth2({
      auth: this.oAuth2Client,
      version: 'v2',
    });
    return new Promise((resolve, reject) => {
      profile.userinfo.get((error, response) => {
        if (error) return reject(null);
        resolve(response.data.email);
      });
    });
  }

  private async signTokenAndSetCookie(payload, response: Response) {
    const token = await this.jwtService.signAsync(payload);
    response
      .cookie('token', token)
      .redirect(this.authOptions.googleAuthOptions.authSuccessRedirectUrl);
  }

  async findUserOrCreate(user: User): Promise<any> {
    const { email, roles } = user;
    return new Promise((resolve, reject) => {
      return this.user
        .findOne({ email })
        .then((result) => {
          if (result)
            return resolve({
              email: result.email,
              roles: result.roles,
              authType: result.authType,
              isActivated: true,
            });
          return this.user
            .create({
              email,
              roles,
              authType: this.authType,
              isActivated: true,
            })
            .then((result) =>
              resolve({
                email: result.email,
                roles: result.roles,
                authType: this.authType,
                isActivated: true,
              }),
            );
        })
        .catch((error) => reject(error));
    });
  }

  getAuthUrl() {
    return {
      url: this.authUrl,
    };
  }

  async login(code: string, res: Response) {
    const email = await this.getGoogleUserEmail(code);
    if (email) {
      let user = await this.findUserOrCreate({
        email,
        roles: this.authOptions.defaultRoles,
        authType: this.authType,
        isActivated: true,
      });
      const payload = {
        email: user.email,
        roles: user.roles,
        authType: user.authtype,
        isActivated: user.isActivated,
      };
      await this.signTokenAndSetCookie(payload, res);
    } else {
      res.redirect(this.authOptions.googleAuthOptions.authFailedRedirectUrl);
    }
  }

  logout(res: Response) {
    res.clearCookie('token');
    return {
      statusCode: 200,
      message: 'logged out',
    };
  }
}
