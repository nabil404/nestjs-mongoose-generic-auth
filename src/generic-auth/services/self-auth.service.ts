import {
  BadRequestException,
  ConflictException,
  HttpException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { JwtService } from '@nestjs/jwt';
import { Response } from 'express';
import { Model } from 'mongoose';

import { ACCOUNT_ACTIVATION, AUTH_OPTIONS, PASSWORD_RESET } from '../constants';
import { AuthOptions } from '../interfaces/auth-options.interface';
import { EmailService } from './email.service';
import { User, UserDocument } from '../schema/user.schema';
import { AuthLSelfCredentialsDto } from '../dto/AuthSelfCredentials.dto';
import { PasswordChangeDto } from '../dto/PasswordChange.dto';
import { PasswordRestDto } from '../dto/PasswordReset.dto';
import { createToken, encryptToken } from '../utils';

@Injectable()
export class SelfAuthService {
  private _defaultRoles;
  private _defaultAuthType = 'self';

  constructor(
    @Inject(AUTH_OPTIONS)
    private authOptions: AuthOptions,
    @InjectModel(User.name) private user: Model<UserDocument>,
    private jwtService: JwtService,
    private emailService: EmailService,
  ) {
    this._defaultRoles = this.authOptions.defaultRoles;
  }

  private async signTokenAndSetCookie(
    payload,
    response: Response,
  ): Promise<string> {
    const jwt = await this.jwtService.signAsync(payload);
    response.cookie('token', jwt);
    return jwt;
  }

  private checkAccountActivation(isActivated) {
    const accountActivation = this.authOptions.accountActivation;
    if (accountActivation && !isActivated) {
      throw new HttpException('account not activated', 400);
    }
  }

  async signUp(
    authCredentialsDto: AuthLSelfCredentialsDto,
    response: Response,
  ): Promise<any> {
    try {
      let activationToken: string;
      const { email, password } = authCredentialsDto;
      const accountActivation = this.authOptions.accountActivation;
      const user = new this.user();
      user.email = email;
      user.roles = this._defaultRoles;
      user.authType = this._defaultAuthType;
      user.password = password;
      user.isActivated = true;
      if (accountActivation) {
        const { token, tokenEncrypted, tokenExpires } = createToken(
          this.authOptions.accountActivationExpires,
        );
        activationToken = token;
        user.isActivated = false;
        user.activationToken = tokenEncrypted;
        user.activationTokenExpires = tokenExpires;
      }
      await user.save();
      const payload: any = {
        _id: user._id,
        email: user.email,
        roles: user.roles,
        authType: user.authType,
        isActivated: user.isActivated,
      };
      await this.signTokenAndSetCookie(payload, response);
      if (accountActivation) {
        await this.emailService.prepareEmailAndSend(
          email,
          ACCOUNT_ACTIVATION,
          activationToken,
        );
        return {
          status: 'success',
          message: 'please verify your account from email',
        };
      }
      return { status: 'success', message: 'account created' };
    } catch (error) {
      if (error.code === 11000) {
        throw new ConflictException('email already exists');
      }
    }
  }

  async signIn(
    authCredentials: AuthLSelfCredentialsDto,
    response: Response,
  ): Promise<any> {
    const { email, password } = authCredentials;
    const user = await this.user.findOne({ email });
    if (!user || !(await user.verifyPassword(password, user.password))) {
      throw new BadRequestException('invalid username or password');
    }
    this.checkAccountActivation(user.isActivated);
    const payload = {
      _id: user._id,
      email: user.email,
      roles: user.roles,
      authType: user.authType,
      isActivated: user.isActivated,
    };
    const token = await this.signTokenAndSetCookie(payload, response);
    return { token };
  }

  async activateAccount(activationToken: string) {
    const encryptedToken = encryptToken(activationToken);
    const user = await this.user.findOne({
      activationToken: encryptedToken,
      activationTokenExpires: {
        $gt: new Date(Date.now()),
      },
    });
    if (!user) throw new BadRequestException('Token is invalid or has expired');
    user.isActivated = true;
    user.activationToken = null;
    user.activationTokenExpires = null;
    await user.save({ validateModifiedOnly: true });
    return { status: 'success', message: 'account activated' };
  }

  async changePassword(
    passwordChangeDto: PasswordChangeDto,
    response: Response,
  ) {
    const { email, oldPassword, newPassword } = passwordChangeDto;
    const user = await this.user.findOne({ email });
    if (!user || !(await user.verifyPassword(oldPassword, user.password))) {
      throw new BadRequestException('Invalid username or password');
    }
    this.checkAccountActivation(user.isActivated);
    user.password = newPassword;
    await user.save();
    response.clearCookie('token');
    return { status: 'success', message: 'password changed' };
  }

  async forgotPassword(email: string) {
    const user = await this.user.findOne({ email });

    if (!user)
      throw new NotFoundException(`user with email ${email} not found`);
    this.checkAccountActivation(user.isActivated);
    const {
      token, //Will be sent to email
      tokenEncrypted,
      tokenExpires,
    } = createToken(this.authOptions.passwordResetExpires);
    await this.emailService.prepareEmailAndSend(email, PASSWORD_RESET, token);
    user.resetToken = tokenEncrypted;
    user.resetExpires = tokenExpires;
    await user.save({ validateBeforeSave: false });
    return { status: 'success', messsage: 'reset token sent to your email' };
  }

  async resetPassword(
    resetToken: string,
    passwordResetDto: PasswordRestDto,
    response: Response,
  ) {
    const encryptedToken = encryptToken(resetToken);
    const user = await this.user.findOne({
      resetToken: encryptedToken,
      resetExpires: {
        $gt: new Date(Date.now()),
      },
    });
    if (!user) throw new BadRequestException('Token is invalid or has expired');
    this.checkAccountActivation(user.isActivated);
    user.password = passwordResetDto.newPassword;
    user.resetExpires = null;
    user.resetToken = null;
    await user.save();
    response.clearCookie('token');
    return { status: 'success', message: 'password reset successful' };
  }
}
