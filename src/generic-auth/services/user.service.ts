import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ACCOUNT_ACTIVATION, AUTH_OPTIONS } from '../constants';
import { AuthOptions } from '../interfaces/auth-options.interface';

import { Model } from 'mongoose';
import { User, UserDocument } from '../schema/user.schema';
import { UpdateRoleDto } from '../dto/UpdateRole.dto';
import { createToken, encryptToken } from '../utils';
import { EmailService } from './email.service';

@Injectable()
export class UserService {
  constructor(
    @Inject(AUTH_OPTIONS)
    private authOptions: AuthOptions,
    @InjectModel(User.name) private user: Model<UserDocument>,
    private emailService: EmailService,
  ) {}

  async getUsers(): Promise<User[]> {
    return await this.user.find().select('-password');
  }

  async updateRole(updateRoleDto: UpdateRoleDto) {
    const { email, role } = updateRoleDto;
    try {
      const user = await this.user.findOne({ email });
      if (!user.roles.includes(role)) user.roles.push(role);
      await user.save();
      return { status: 'success', message: 'role updated' };
    } catch (error) {
      return { status: 'failed', message: error };
    }
  }

  async resetActivationToken(email: string) {
    const user = await this.user.findOne({ email });
    if (!user)
      throw new NotFoundException(
        `user with email: ${email} not found in database`,
      );
    if (user.isActivated)
      return { status: 'failed', message: 'account already activated' };
    const { token, tokenEncrypted, tokenExpires } = createToken(
      this.authOptions.accountActivationExpires,
    );
    user.activationToken = tokenEncrypted;
    user.activationTokenExpires = tokenExpires;
    await user.save();
    await this.emailService.prepareEmailAndSend(
      user.email,
      ACCOUNT_ACTIVATION,
      token,
    );
    return {
      status: 'success',
      message: 'new activation token is sent to email',
    };
  }
}
