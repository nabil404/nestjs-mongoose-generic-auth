import { Inject, Injectable } from '@nestjs/common';
import { createTransport } from 'nodemailer';
import { compile } from 'handlebars';
import { ACCOUNT_ACTIVATION, AUTH_OPTIONS, PASSWORD_RESET } from '../constants';
import { AuthOptions } from '../interfaces/auth-options.interface';
import { ActivationTemplate, ResetTemplate } from '../templates';

@Injectable()
export class EmailService {
  private _emailFrom: string;
  private _emailHost: string;
  private _emailPort: number;
  private _emailPassword: string;
  private _emailUser: string;

  constructor(
    @Inject(AUTH_OPTIONS)
    private authOptions: AuthOptions,
  ) {
    const {
      emailFrom,
      emailHost,
      emailPort,
      emailPassword,
      emailUser,
    } = this.authOptions.emailOptions;
    this._emailFrom = emailFrom;
    this._emailHost = emailHost;
    this._emailPort = emailPort;
    this._emailUser = emailUser;
    this._emailPassword = emailPassword;
  }

  private createTransport() {
    return createTransport({
      host: this._emailHost,
      port: this._emailPort,
      auth: {
        user: this._emailUser,
        pass: this._emailPassword,
      },
    });
  }

  async sendEmail(to: string, subject: string, template: string) {
    const mailOptions: any = {
      to,
      subject,
      from: this._emailFrom,
      html: template,
    };
    await this.createTransport().sendMail(mailOptions);
  }

  async prepareEmailAndSend(to: string, emailType: string, token: string) {
    if (emailType === ACCOUNT_ACTIVATION) {
      const subject =
        this.authOptions.emailOptions.accountActivationSubject ||
        'Account activation';
      const activationUrl = this.authOptions.emailOptions.accountActivationUrl.replace(
        '{{token}}',
        token,
      );
      const template =
        this.authOptions.emailOptions.accountActivationTemplate ||
        ActivationTemplate;
      const renderedTemplate = this.renderEmailTemplate(template, {
        activationUrl,
      });
      await this.sendEmail(to, subject, renderedTemplate);
    } else if (emailType === PASSWORD_RESET) {
      const subject =
        this.authOptions.emailOptions.accountActivationSubject ||
        'Password reset';
      const resetUrl = this.authOptions.emailOptions.passwordResetUrl.replace(
        '{{token}}',
        token,
      );
      const template =
        this.authOptions.emailOptions.passwordResetTemplate || ResetTemplate;
      const renderedTemplate = this.renderEmailTemplate(template, {
        resetUrl,
      });
      await this.sendEmail(to, subject, renderedTemplate);
    }
  }

  renderEmailTemplate(source: string, data: Object) {
    const template = compile(source);
    return template(data);
  }
}
