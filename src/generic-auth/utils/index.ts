const crypto = require('crypto');

export function encryptToken(token: string) {
  return crypto.createHash('sha256').update(token).digest('hex');
}

export function createToken(expires?: number) {
  const token = crypto.randomBytes(32).toString('hex');
  const tokenEncrypted = this.encryptToken(token);
  if (expires) {
    const tokenExpires = new Date(Date.now() + 1000 * 60 * expires);
    return { token, tokenEncrypted, tokenExpires };
  }

  return { token, tokenEncrypted };
}
