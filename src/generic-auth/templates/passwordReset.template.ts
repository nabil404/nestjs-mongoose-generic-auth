export const PasswordResetTemplate = `
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
</head>
<body> 
<h1>Welcome!</h1>   
    <a href="{{resetUrl}}" target="_blank">Reset Password</a></td>
    <p>If that doesn't work, copy and paste the following link in your browser: {{resetUrl}}</p> 
</body>

</html>
`;
