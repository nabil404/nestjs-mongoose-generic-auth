import { AccountActivationTemplate } from './accountActivation.template';
import { PasswordResetTemplate } from './passwordReset.template';
export const ActivationTemplate = AccountActivationTemplate;
export const ResetTemplate = PasswordResetTemplate;