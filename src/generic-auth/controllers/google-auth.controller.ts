import { Controller, Get, Res, Query, UseGuards } from '@nestjs/common';
import { Response } from 'express';
import { GoogleAuthService } from '../services/google-auth.service';
import { JwtAuthGuard } from '../guards/jwtAuth.guard';

@Controller('google')
export class GoogleAuthController {
  constructor(private googleAuthService: GoogleAuthService) {}

  @Get('getauthurl')
  getAuthUrl() {
    return this.googleAuthService.getAuthUrl();
  }

  @Get('login')
  async login(
    @Res({ passthrough: true }) res: Response,
    @Query('code') code: string,
  ) {
    await this.googleAuthService.login(code, res);
  }

  @Get('logout')
  @UseGuards(JwtAuthGuard)
  logout(@Res({ passthrough: true }) res: Response) {
    return this.googleAuthService.logout(res);
  }
}
