import {
  Body,
  Controller,
  Get,
  Patch,
  Post,
  Res,
  UseGuards,
  ValidationPipe,
  Param,
} from '@nestjs/common';
import { Response } from 'express';
import { AuthLSelfCredentialsDto } from '../dto/AuthSelfCredentials.dto';
import { SelfAuthService } from '../services/self-auth.service';
import { JwtAuthGuard } from '../guards/jwtAuth.guard';
import { CheckOptions } from '../decorators/checkOptions.decorator';
import { PasswordChangeDto } from '../dto/PasswordChange.dto';
import { PasswordRestDto } from '../dto/PasswordReset.dto';

@Controller('auth')
export class SelfAuthController {
  constructor(private selfAuthService: SelfAuthService) {}

  @Post('signup')
  async signUp(
    @Body(ValidationPipe) authCredentialsDto: AuthLSelfCredentialsDto,
    @Res({ passthrough: true }) response: Response,
  ): Promise<any> {
    return await this.selfAuthService.signUp(authCredentialsDto, response);
  }

  @Post('signin')
  async signin(
    @Body(ValidationPipe) authCredentialsDto: AuthLSelfCredentialsDto,
    @Res({ passthrough: true }) response: Response,
  ): Promise<any> {
    return await this.selfAuthService.signIn(authCredentialsDto, response);
  }

  @UseGuards(JwtAuthGuard)
  @Get('signout')
  signout(@Res({ passthrough: true }) response: Response) {
    response.clearCookie('jwt');
    return { message: 'signed out' };
  }

  @UseGuards(JwtAuthGuard)
  @Patch('changePassword')
  async changePassword(
    @Body(ValidationPipe) passwordChangeDto: PasswordChangeDto,
    @Res({ passthrough: true }) response: Response,
  ) {
    return this.selfAuthService.changePassword(passwordChangeDto, response);
  }

  @UseGuards(JwtAuthGuard)
  @CheckOptions('passwordReset')
  @Post('forgotPassword')
  async forgotPassword(@Body('email') email: string) {
    return await this.selfAuthService.forgotPassword(email);
  }

  @UseGuards(JwtAuthGuard)
  @CheckOptions('passwordReset')
  @Post('resetPassword/:resetToken')
  async resetPassword(
    @Param('resetToken') resetToken: string,
    @Body(ValidationPipe) passwordResetDto: PasswordRestDto,
    @Res({ passthrough: true }) response: Response,
  ) {
    return await this.selfAuthService.resetPassword(
      resetToken,
      passwordResetDto,
      response,
    );
  }

  @CheckOptions('accountActivation')
  @Post('activate/:activationToken')
  async activateAccount(@Param('activationToken') activationToken: string) {
    return await this.selfAuthService.activateAccount(activationToken);
  }
}
