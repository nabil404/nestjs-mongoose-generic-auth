import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { GetUser } from '../decorators/getUser.decorator';
import { JwtAuthGuard } from '../guards/jwtAuth.guard';
import { User } from '../schema/user.schema';
import { UserService } from '../services/user.service';
import { Role } from '../decorators/role.decorator';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Role('super-admin', 'admin')
  @UseGuards(JwtAuthGuard)
  @Get()
  async getUsers() {
    return await this.userService.getUsers();
  }

  @UseGuards(JwtAuthGuard)
  @Get('me')
  me(@GetUser() user: User) {
    return user;
  }

  @Post('resetActivationToken')
  async resetActivationToken(@Body('email') email: string) {
    return await this.userService.resetActivationToken(email);
  }
}
