import { NotFoundException } from '@nestjs/common';
import {
  Injectable,
  CanActivate,
  ExecutionContext,
  Inject,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { AUTH_OPTIONS } from '../constants';
import { AuthOptions } from '../interfaces/auth-options.interface';

Injectable();
export class RouteGuard implements CanActivate {
  constructor(
    @Inject(AUTH_OPTIONS)
    private authOptions: AuthOptions,
    private reflector: Reflector,
  ) {}

  canActivate(context: ExecutionContext) {
    const Options = this.reflector.getAllAndOverride<string[]>('authOptions', [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!Options) return true;

    const request = context.switchToHttp().getRequest();
    this.checkOptions(request, Options);
    return true;
  }

  private checkOptions(request: any, Options: string[]) {
    Options.forEach((option) => {
      if (!this.authOptions[option])
        throw new NotFoundException(`Cannot ${request.method} ${request.url}`);
    });
  }
}
