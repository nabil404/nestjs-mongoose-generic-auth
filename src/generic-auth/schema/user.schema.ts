import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as bcrypt from 'bcrypt';

@Schema()
export class User {
  @Prop({ required: true, unique: true })
  email: string;

  @Prop({
    required: true,
    unique: false,
  })
  roles: string[];

  @Prop({
    required: true,
  })
  authType: string;

  @Prop()
  isActivated?: boolean;

  @Prop({ type: String })
  password?: string;

  @Prop()
  activationToken?: string;

  @Prop()
  activationTokenExpires?: Date;

  @Prop()
  resetToken?: string;

  @Prop()
  resetExpires?: Date;

  verifyPassword?: Function;
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.pre('save', async function (next) {
  if (this.get('authType') !== 'self') return;
  if (this.isModified('password')) {
    const password = await bcrypt.hash(this.get('password'), 10);
    this.set('password', password);
  }
  next();
});

UserSchema.methods.verifyPassword = async (
  candidatePassword: string,
  userPassword: string,
): Promise<Boolean> => await bcrypt.compare(candidatePassword, userPassword);

export type UserDocument = User & Document;
