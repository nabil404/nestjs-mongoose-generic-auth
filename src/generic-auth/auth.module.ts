import { DynamicModule, Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';

import { AUTH_OPTIONS } from './constants';

import { UserService } from './services/user.service';
import { SelfAuthService } from './services/self-auth.service';
import { GoogleAuthService } from './services/google-auth.service';

import { UserController } from './controllers/user.controller';
import { SelfAuthController } from './controllers/self-auth.controller';
import { GoogleAuthController } from './controllers/google-auth.controller';

import { AuthOptions } from './interfaces/auth-options.interface';
import { defaultAuthOptions } from './config/default-auth-options';

import { User, UserSchema } from './schema/user.schema';

import { JwtStrategy } from './services/jwt.strategy';
import { EmailService } from './services/email.service';

@Module({})
export class AuthModule {
  public static register(authOptions: AuthOptions): DynamicModule {
    const options = this.setOptions(authOptions);
    const authService = this.selectAuthServices(options.authenticationtype);
    const authControllers = this.selectAuthControllers(
      options.authenticationtype,
    );
    return {
      module: AuthModule,
      imports: [
        PassportModule.register({ ...options.passportModuleOptions }),
        JwtModule.register({ ...options.jwtModuleOptions }),
        MongooseModule.forFeature(
          [
            {
              name: User.name,
              schema: UserSchema,
            },
          ],
          options.connectionName,
        ),
      ],
      controllers: [...authControllers, UserController],
      providers: [
        {
          provide: AUTH_OPTIONS,
          useValue: options,
        },
        ...authService,
        UserService,
        JwtStrategy,
        EmailService,
      ],
      exports: [...authService],
    };
  }

  private static setOptions(authOptions: AuthOptions): AuthOptions {
    let options = { ...defaultAuthOptions, ...authOptions };
    if (!options.authenticationtype.includes('self')) {
      options.passwordReset = false;
      options.accountActivation = false;
    }
    if (
      options.authenticationtype.includes('google') &&
      !options.googleAuthOptions
    ) {
      throw new Error('googleAuthOptions must be provided');
    }
    if (
      (options.passwordReset || options.accountActivation) &&
      !options.emailOptions
    )
      throw new Error('email options must be provided');

    if (options.passwordReset && !options.emailOptions.passwordResetUrl) {
      throw new Error('passwordResetUrl must be provided in emailOptions');
    }

    if (
      options.accountActivation &&
      !options.emailOptions.accountActivationUrl
    ) {
      throw new Error('accountActivationUrl must be provided in emailOptions');
    }
    return { ...options };
  }

  private static selectAuthServices(authenticationType): any[] {
    const selectedAuthServices = [];
    authenticationType.forEach((type: any) => {
      if (type === 'self') {
        selectedAuthServices.push(SelfAuthService);
      } else if (type === 'google') {
        selectedAuthServices.push(GoogleAuthService);
      }
    });
    return selectedAuthServices;
  }

  private static selectAuthControllers(authenticationType): any[] {
    const selectedAuthControllers = [];
    authenticationType.forEach((type: any) => {
      if (type === 'self') {
        selectedAuthControllers.push(SelfAuthController);
      } else if (type === 'google') {
        selectedAuthControllers.push(GoogleAuthController);
      }
    });
    return selectedAuthControllers;
  }
}
