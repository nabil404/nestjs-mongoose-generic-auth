import { AuthOptions } from '../interfaces/auth-options.interface';

export const defaultAuthOptions: AuthOptions = {
  authenticationtype: ['self'],
  passportModuleOptions: { defaultStrategy: 'jwt' },
  jwtModuleOptions: {},
  defaultRoles: ['guest'],
  passwordReset: true,
  passwordResetExpires: 5,
  accountActivation: false,
  accountActivationExpires: 5,
  emailOptions: null,
  googleAuthOptions: null,
};
