import { IsNotEmpty, IsString, IsEmail } from 'class-validator';

export class UpdateRoleDto {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  role: string;
}
