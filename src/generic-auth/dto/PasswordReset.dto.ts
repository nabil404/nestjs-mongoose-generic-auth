import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class PasswordRestDto {
  @IsNotEmpty()
  @IsString()
  @MinLength(8)
  @MaxLength(20)
  newPassword: string;
}
