import { applyDecorators, SetMetadata, UseGuards } from '@nestjs/common';
import { RouteGuard } from '../guards/route.guard';

export function CheckOptions(...options: string[]) {
  return applyDecorators(
    SetMetadata('authOptions', options),
    UseGuards(RouteGuard),
  );
}
